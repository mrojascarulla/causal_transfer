# Copyright (c) 2014 - 2016  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import scipy as sc

def linear(X,params):
	alphaC = params[0]
	return alphaC*X

def zero(X,params):
	return 0.

class dataset(object):
	def __init__(self,X,Y,nEx):
		self.XTrain = X
		self.YTrain = Y
		self.XTest = X
		self.YTest = Y
		self.XValid = X
		self.YValid = Y
		self.nEx = nEx
		self.numTasks = np.size(nEx)
		self.numPredictors = X.shape[1]

class gaussianMTL(object):

	def __init__(self, numCauses, numPredictors, numTasks, maxExamples,fs, muMeans, SMeans, ac,ae, coefVCV):
                if len(ac)>1:
                        alphaC = np.random.uniform(ac[0], ac[1], numCauses)
                else:
                        alphaC = ac[0]

		params = [alphaC]

		self.alphaC = alphaC
		self.numTasks = numTasks
		self.maxExamples = maxExamples
		self.numCauses = numCauses
		self.numPredictors = numPredictors
		self.eps = SMeans[0]
                self.numNoise = numPredictors - 2*numCauses

		nEx = np.array(numTasks*[maxExamples])
		self.nEx = nEx
                if self.numNoise != 0:
                        numEffects = numPredictors - numCauses -  self.numNoise
                else:
                        numEffects = numPredictors - numCauses
		self.totalEx = np.sum(nEx)
		self.numEffects = numEffects

		def randomCovMat(mu,sigma, nElemX, nElemY):
			Sigma = np.random.uniform(-mu,mu, (nElemX,nElemY))
                        
			if nElemX == nElemY:
                                Sigma = 1./nElemX*np.dot(Sigma,Sigma.T)
				return Sigma
			else:
				return Sigma

		def sampleFromModel(mu,Sigma, alphaE, numEffects,ex):
                        
			XS = np.random.multivariate_normal(mu,Sigma[0],nEx[task])
			Y = np.sum(fs[0](XS,params), axis = 1) + np.random.normal(0,SMeans[0],nEx[task])
                        
			XN = fs[1](Y[:,np.newaxis],[alphaE]) + coefVCV[0]*np.random.multivariate_normal(np.zeros(numEffects), Sigma[1], nEx[task]) + coefVCV[1]*np.dot(XS,Sigma[2].T)

                        if self.numNoise < 0:
                                noise = np.random.multivariate_normal(np.zeros(self.numNoise), np.eye(self.numNoise), nEx[task])
                        
                                return np.concatenate([XS,XN,noise],axis = 1), Y[:,np.newaxis]
                        else:
                                return np.concatenate([XS,XN], axis = 1), Y[:,np.newaxis]

		self.XTrain = np.zeros((self.totalEx,self.numPredictors-self.numNoise))
		self.YTrain = np.zeros((self.totalEx,1))
		self.XValid = np.zeros((self.totalEx,self.numPredictors-self.numNoise))
		self.YValid = np.zeros((self.totalEx,1))
		self.XTest = np.zeros((self.totalEx,self.numPredictors-self.numNoise))
		self.YTest = np.zeros((self.totalEx,1))

		predInd = 0
		_Sigma, _SigmaE, _SigmaCross, _alphaE  = [], [], [], []
		_SigmaT, _SigmaET, _SigmaCrossT, _alphaET  = [], [], [], []

		mu = np.zeros(numCauses)
		nExcum = np.cumsum(self.nEx)

		for task in range(numTasks):

			#Generate mean vector and covariance matrix

			_alphaE.append(np.random.uniform(ae[0],ae[1],numEffects))
			_alphaET.append(np.random.uniform(ae[0] , ae[1],numEffects))

                _Sigma.append(randomCovMat(muMeans[1], SMeans[2], numCauses, numCauses))
                _Sigma = numTasks*_Sigma

                _SigmaE.append(randomCovMat(muMeans[2], SMeans[3], numEffects, numEffects))
                _SigmaE = numTasks*_SigmaE

                _SigmaCross.append(randomCovMat(muMeans[3], SMeans[4], numEffects, numCauses))
                _SigmaCross = numTasks*_SigmaCross

                _SigmaT.append(randomCovMat(muMeans[1], SMeans[2], numCauses, numCauses))
                _SigmaT = numTasks*_Sigma

                _SigmaET.append(randomCovMat(muMeans[2], SMeans[3], numEffects, numEffects))
                _SigmaET = numTasks*_SigmaE

                _SigmaCrossT.append(randomCovMat(muMeans[3], SMeans[4], numEffects, numCauses))
                _SigmaCrossT = numTasks*_SigmaCross

		for task in range(numTasks):

			X,Y = sampleFromModel(mu, [_Sigma[task],_SigmaE[task], _SigmaCross[task]], _alphaE[task], numEffects, nEx[task])
			XValid,YValid = sampleFromModel(mu, [_Sigma[task], _SigmaE[task], _SigmaCross[task]], _alphaE[task], numEffects, nEx[task])

			self.XTrain[predInd:nExcum[task],:] = X
			self.YTrain[predInd:nExcum[task]] = Y
			self.XValid[predInd:nExcum[task],:] = XValid
			self.YValid[predInd:nExcum[task]] = YValid


			X,Y = sampleFromModel(mu, [_SigmaT[task], _SigmaET[task], _SigmaCrossT[task]], _alphaET[task], numEffects, nEx[task])
			self.XTest[predInd:nExcum[task],:] = X
			self.YTest[predInd:nExcum[task]] = Y

			predInd = nExcum[task]

                if self.numNoise != 0:

                        noise_train = np.random.multivariate_normal(np.zeros(self.numNoise), np.eye(self.numNoise), self.XTrain.shape[0])
                        self.XTrain = np.append(self.XTrain, noise_train, axis = 1)

                        noise_test = np.random.multivariate_normal(np.zeros(self.numNoise), np.eye(self.numNoise), self.XTest.shape[0])
                        self.XTest = np.append(self.XTest, noise_test, axis = 1)

                        noise_valid = np.random.multivariate_normal(np.zeros(self.numNoise), np.eye(self.numNoise), self.XValid.shape[0])
                        self.XValid = np.append(self.XValid, noise_valid, axis = 1)

