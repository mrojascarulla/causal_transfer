#!/usr/bin/env python
# Copyright (c) 2014 - 2016  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import utils 
from sklearn import linear_model

#----------------------------------------------------------------

#   train_x: (n_examples,n_predictors) matrix with training features
#   train_y: (n_examples,1) vector with training targets
#   n_ex:    (n_tasks, ) vector with number of training examples per task
#   delta:   level for the statistical test
#   use_hsic: True for using HSIC in the independence test, False for Levene. Default False

#   out: invariant subset computed using Algorithm 1. 
#------------------------------------------------------------------

def subset(train_x, train_y, n_ex, delta, use_hsic = False):

    n_examples = train_x.shape[0]
    n_predictors = train_x.shape[1]
    
    dataset = utils.dataset(train_x, train_y, n_ex)
    mse_subsets, mse_subsets_t, subset = utils.best_subset_regression(dataset, use_hsic, delta)

    return subset


#----------------------------------------------------------------

#   train_x: (n_examples,n_predictors) matrix with training features
#   train_y: (n_examples,1) vector with training targets
#   test_x: (n_examples_test,n_predictors) matrix with available test features
#   test_y: (n_examples,1) vector with available test targets
#   n_ex:    (n_tasks, ) vector with number of training examples per task in training
#   delta:   level for the statistical test
#   use_hsic: True for using HSIC in the independence test, False for Levene. Default False

#   out: optimal coefficients (beta_S, beta_N) computed using eq (9) and (10).
#------------------------------------------------------------------

def subset_plus(train_x, train_y, test_x, test_y, n_ex, delta, use_hsic = False):

    n_examples = train_x.shape[0]
    n_predictors = train_x.shape[1]
    
    train_x = np.append(train_x, test_x, axis = 0)
    train_y = np.append(train_y, test_y, axis = 0)
    n_ex = np.append(n_ex,test_x.shape[0])

    dataset = utils.dataset(train_x, train_y, n_ex)
    mse_subsets, mse_subsets_t, subset = utils.best_subset_regression(dataset, use_hsic, delta)    
    if subset.size > 0:
        regr = linear_model.LinearRegression()
        regr.fit(train_x[:,subset], train_y)
        pred = regr.predict(train_x[:,subset])
        alpha = regr.coef_
    else:
        pred = np.mean(train_y)
        alpha = np.array([])

    epsilon = np.mean((train_y-pred)**2)
    mask = np.ones(dataset.numPredictors, dtype = bool)
    mask[subset] = False

    if subset.size == dataset.numPredictors:
        return alpha, 0, subset, mask
    else:
        beta_S, beta_N = utils.optimal_coefs(alpha, epsilon, test_x[:,subset], test_x[:,mask], test_y.flatten(), test_x.shape[0], subset.size, dataset.numPredictors-subset.size, 0.000001)

    return beta_S, beta_N, subset, mask
