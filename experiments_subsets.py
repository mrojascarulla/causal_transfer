#!/usr/bin/env python
# Copyright (c) 2014 - 2016  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import scipy as sp
from scipy import io
import generate_gaussian_data
from sklearn import linear_model
from matplotlib import pyplot as pl
import cPickle
import matplotlib as mpl
import seaborn as sns
import matplotlib
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
import itertools

import utils
import subset_search

sns.set(color_codes = True)
sns.set_style('white')

mpl.rc("figure",facecolor="white")
mpl.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
mpl.rc('text', usetex=True)

def figure1():

        np.random.seed(1234)
        sig = 0.5
        num_iters = 10000

        index = 0
        errors = np.zeros((2,num_iters))
        errors_causal = np.zeros((2,num_iters))

        for i in range(num_iters):
                
                #sample an inter task std
                sigma = np.sqrt(np.random.uniform(0.2,8))
                
                if sigma**2<4:
                        a = np.random.uniform(0,1)
                        if a<0.3:
                                continue
                
                sx = np.sqrt(np.random.uniform(1.5,2.5))
                sz = np.sqrt(np.random.uniform(0.3,0.8))

                alpha = np.random.normal(0,1)
                #sample coef for both tasks
                gamma_1 = np.random.normal(0,sigma)
                gamma_2 = np.random.normal(0,sigma)

                gamma_bar = gamma_1 + gamma_2
                gamma2_bar = gamma_1**2 + gamma_2**2
                Vy = alpha**2*sx**2+sig**2
                g2 = 2*gamma_bar*sig**2
                g2 /= (2*Vy*gamma2_bar + 4*sz**2- alpha**2*sx**2*gamma_bar**2)
                g1 = alpha-alpha/2*gamma_bar*g2

                error_pool = g1**2*sx**2 + g2**2*(Vy*sigma**2+sz**2)
                error_pool += Vy-2*g1*alpha*sx**2

                errors[0,i] = error_pool
                errors_causal[0, i] = sig**2

                errors[1,i] = sigma**2
                errors_causal[1, i] = sigma**2

        Sigma_2 = np.arange(0.2,7.1,0.01)
        pl.plot(errors[1,:], errors[0,:], 'ro', markersize = 3, alpha = 0.6)
        pl.plot(Sigma_2, Sigma_2.size*[sig**2], 'bo', markersize = 3, alpha = 1)
        pl.xlabel(r'$\Sigma^2$', fontsize = 22)
        pl.ylabel(r'Expected error on test task', fontsize = 22)
        pl.legend(['Pooled','Subsets'], loc = 'upper left', fontsize = 22)
        pl.xlim([0.2,7])
        pl.ylim(-0.8)
        pl.xticks(fontsize = 20)
        pl.yticks(fontsize = 20)
        pl.savefig('figures/exp_scatter.pdf')
        pl.show()


#Running this code recovers the graph from Figure 3 (right)
def figure3_tl():

	np.random.seed(1234)
	ex_task = 600
	num_tasks = 10
	num_predictors = 10
	num_causes = 5
        fs = [generate_gaussian_data.linear,generate_gaussian_data.linear,generate_gaussian_data.zero]
	mu_means = np.array([0,2.,0.01,0.021])
	s_means = np.array([2,2.,0.1,0.1,0.0001])
	ac = [-1.,2.5]
	gamma = [-1.5,1.5]
	coefVCV = [1,0]
	num_simuls = 10

	mse_error = np.zeros((3,num_tasks-2,num_simuls))
	mse_test_error = np.zeros((6,num_tasks-2,num_simuls))

        #level for indep test
        delta = 0.05

        #Set to True for running the algorithm, False to simply plot saved results
        run_algorithm = True
        save = False

        if run_algorithm:        
                # Generate the datasets for each iteration
                datasets = []
                for i in range(num_simuls):
                        dataset = generate_gaussian_data.gaussianMTL(num_causes, num_predictors, num_tasks, ex_task, fs, mu_means, s_means,ac, gamma ,coefVCV)
                        datasets.append(dataset)

                for i in range(num_simuls):
                        dataset = datasets[i]
                        mse_error, mse_test_error = utils.tl_run_experiment(dataset, mse_error, mse_test_error, i, delta)

                if save:
                        error_file = 'save/tl_errors_'+str(ex_task)
                        with open(error_file, 'wb') as f:
                                cPickle.dump(mse_test_error, f)
        else:
                with open('tl_errors_600', 'rb') as f:
                        mse_test_error = cPickle.load(f)

                with open('save/errors_tl_dica2', 'rb') as f:
                        dica2 = cPickle.load(f)
                with open('save/errors_tl_dica5', 'rb') as f:
                        dica5 = cPickle.load(f)

                
                mse_test_error[-1, :, :] = dica2
                mse_test_error = np.append(mse_test_error, dica5[np.newaxis, :, :], axis = 0)
                mse_test_error = mse_test_error[:,:,:]

        #Plot and save array of errors

	labels = [r"$\beta^{pool}$", r"$\beta^{\hat{S}}$", r"$\beta^{cau}$",r"$\beta^{mean}$",r"$\beta^{DICA, 2}$", r"$\beta^{DICA,5}$"]
	colors = ['blue', 'green', 'orange', 'purple','black', 'brown']
        colors_band = ['#9999ff',  '#99ff99', '#ffcc99', '#b3ffcc','#cccccc', '#ffcc99']
	file_name = 'figures/tl.pdf'
	utils.plot_mse(num_tasks,mse_error,np.arange(3,num_tasks+1),np.log(mse_test_error),labels,colors, file_name, colors_band, r'Number $D$ of training tasks')
        

#Running this code recovers the graph from Figure 3 (left)
def figure3_mtl():

        np.random.seed(1234)
	ex_task = 600
	num_tasks = 6#10
	num_predictors = 6#20
	num_causes =3# 5
        
        #Number of examples in the test task at training
        samp_array = np.arange(2,50,2)
	fs = [generate_gaussian_data.linear,generate_gaussian_data.linear,generate_gaussian_data.zero]
	mu_means = np.array([0,2.,0.01,0.021])
	s_means = np.array([2,2.,0.1,0.1,0.0001])
	ac = [-1,2.5]
	gamma = [-1.5,1.5]

	coefVCV = [1,0]
	num_simuls = 20

	mse_error = np.zeros((5,samp_array.size,num_simuls))
	mse_test_error = np.zeros((5,samp_array.size,num_simuls))
        
        datasets = []

        #Confidence level for independence test
        delta = 0.05
        run_algorithm = True
        save = False

        if run_algorithm:

                for i in range(num_simuls):
                        dataset = generate_gaussian_data.gaussianMTL(num_causes, num_predictors, num_tasks, ex_task, fs, mu_means, s_means,ac, gamma,coefVCV)
                        datasets.append(dataset)

                for i in range(num_simuls):
                        dataset = datasets[i]
                        mse_error, mse_test_error = utils.mtl_run_experiment(dataset, delta, mse_error, mse_test_error, i, samp_array)

                if save:
                        file_mtl_errors = 'mtl_errors'
                        with open(file_mtl_errors, 'wb') as f:
                                cPickle.dump(mse_test_error, f)
        else:
                file_mtl_errors = 'mtl_errors'
                with open(file_mtl_errors, 'rb') as f:
                        errors = cPickle.load(f)
                        
                file_mtl = 'save/mtl_synt_testing'
                with open(file_mtl, 'rb') as f:
                        mtl = cPickle.load(f)

                errors[6,:,:] = mtl

	labels = [r"$\beta^{pool}$", r"$\beta^{\hat{S}+}$", r"$\beta^{cau+}$",r"$\beta^{dom}$", r"$\beta^{MTL}$"]
	colors = ['blue', 'green', 'orange', 'red','black']
        colors_band = ['#9999ff',  '#99ff99', '#ffcc99', '#ff9999','#cccccc']

	fileName = 'mtl.pdf'

	utils.plot_mse(num_tasks,mse_error,samp_array,np.log(mse_test_error),labels,colors, fileName, colors_band, r'Number $n_T$ of examples in $T$')

figure1()
