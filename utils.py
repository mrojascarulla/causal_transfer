#!/usr/bin/env python
# Copyright (c) 2014 - 2016  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import scipy as sp
import itertools
from sklearn import linear_model
from matplotlib import pylab as pl

def plot_mse(numTasks,evArray,xaxis,evTestArray,labels, colors, fileName, colors_band = [], xlabel = ''):

        if len(colors_band)==0:
            colors_band = colors

	fig,ax0 = pl.subplots(1,1)
	for est in range(len(labels)):

            yerr = np.std(evTestArray[est,:,:],axis = 1)
            y = np.mean(evTestArray[est,:,:],axis = 1)
            pl.plot(xaxis, y, markersize = 5,color=colors[est], marker = 'o')

            pl.fill_between(xaxis, y-yerr, y+ yerr, alpha=0.35, edgecolor=colors_band[est], facecolor=colors_band[est], interpolate = True)
        
	ax0.legend(labels, loc = 'best', prop={'size':15}, ncol = 2)
	ax0.set_xlabel(xlabel, fontsize = 18)
	ax0.set_ylabel(r'Log $\hat{MSE}$', fontsize = 18)
        pl.xlim([xaxis[0], xaxis[-1]])
        #pl.ylim([-16, 3])
	pl.savefig(fileName)
	pl.show()


class dataset(object):
	def __init__(self,X,Y,nEx):
		self.XTrain = X
		self.YTrain = Y
		self.XTest = X
		self.YTest = Y
		self.XValid = X
		self.YValid = Y
		self.nEx = nEx
		self.numTasks = np.size(nEx)
		self.numPredictors = X.shape[1]


# Functions for HSIC and Levene
def levene_pval(Residual,nEx, numR):
	
	prev = 0 
	n_ex_cum = np.cumsum(nEx)

	for j in range(numR):

		r1 = Residual[prev:n_ex_cum[j],:]

		if j == 0:
			residTup = (r1,)

		else:
			residTup = residTup + (r1,)

		prev = n_ex_cum[j]

	return residTup

def np_getDistances(x,y):
	K = (x[:,:, np.newaxis] - y.T)
	return np.linalg.norm(K,axis = 1)

def np_gaussian_kernel(x,y, beta=0.1):
    K = np_outer_substract(x,y)
    return np.exp( -beta * np.linalg.norm(K, axis=1))

def mat_hsic(X,nEx):

	nExCum = np.cumsum(nEx)
	domains = np.zeros((np.sum(nEx),np.sum(nEx)))
	currentIndex = 0

	for i in range(nEx.size):

		domains[currentIndex:nExCum[i], currentIndex:nExCum[i]] = np.ones((nEx[i], nEx[i]))
		currentIndex = nExCum[i]

	return domains

def numpy_GetKernelMat(X,sX):

	Kernel = (X[:,:, np.newaxis] - X.T).T
	Kernel = np.exp( -1./(2*sX) * np.linalg.norm(Kernel, axis=1))

	return Kernel

def numpy_HsicGammaTest(X,Y, sigmaX, sigmaY, DomKer = 0):

	n = X.T.shape[1]

	KernelX = numpy_GetKernelMat(X,sigmaX)

	KernelY = DomKer

	coef = 1./n
	HSIC = coef**2*np.sum(KernelX*KernelY) + coef**4*np.sum(KernelX)*np.sum(KernelY) - 2*coef**3*np.sum(np.sum(KernelX,axis=1)*np.sum(KernelY, axis=1))
	
	#Get sums of Kernels
	KXsum = np.sum(KernelX)
	KYsum = np.sum(KernelY)

	#Get stats for gamma approx

	xMu = 1./(n*(n-1))*(KXsum - n)
	yMu = 1./(n*(n-1))*(KYsum - n)
	V1 = coef**2*np.sum(KernelX*KernelX) + coef**4*KXsum**2 - 2*coef**3*np.sum(np.sum(KernelX,axis=1)**2)
	V2 = coef**2*np.sum(KernelY*KernelY) + coef**4*KYsum**2 - 2*coef**3*np.sum(np.sum(KernelY,axis=1)**2)

	meanH0 = (1. + xMu*yMu - xMu - yMu)/n
	varH0 = 2.*(n-4)*(n-5)/(n*(n-1.)*(n-2.)*(n-3.))*V1*V2

	#Parameters of the Gamma
	a = meanH0**2/varH0
	b = n * varH0/meanH0

	return n*HSIC, a, b

#Select top ten predictors from Lasso
def lasso_alpha_search_synt(X,Y):

    exit_loop = False
    alpha_lasso = 0.2
    step = 0.02
    num_iters = 1000
    count = 0

    while(not exit_loop and count < num_iters):
            count = count + 1

            regr = linear_model.Lasso(alpha = alpha_lasso)
            regr.fit(X,Y.flatten())
            zeros =  np.where(np.abs(regr.coef_) < 0.00000000001)

            nonzeros = X.shape[1]-zeros[0].shape[0]

            if(nonzeros >= 8 and nonzeros<10):
                    exit_loop = True
            if nonzeros<8:
                    alpha_lasso -= step
            else:
                    step /= 2
                    alpha_lasso += step


    mask = np.ones(X.shape[1],dtype = bool)
    mask[zeros] = False
    genes = []
    index_mask = np.where(mask == True)[0]

    return mask

#Return the estimated invariant subset
def best_subset_regression(dataset, use_hsic, delta):
	
	nEx = dataset.nEx[0:dataset.numTasks]
	nExCum = np.cumsum(nEx)
	ntasksArray = np.array([dataset.numTasks])

	mse = np.zeros((2,ntasksArray.size))
	mse_t = np.zeros((2,ntasksArray.size))
	best_subset = []
        
	test_x,test_y   = dataset.XTest, dataset.YTest
        nExV = nEx

        best_mse, best_mse_t = 100000, 0

        train_x,train_y = dataset.XTrain, dataset.YTrain
        valid_x,valid_y = dataset.XValid, dataset.YValid

        rang = np.arange(dataset.numPredictors)

        for i in range(1,rang.size+1):
                for s in itertools.combinations(rang,i):

                        currentIndex = rang[np.array(s)]
                        regr = linear_model.LinearRegression()

                        regr.fit(train_x[:,currentIndex],train_y.flatten())

                        pred = regr.predict(valid_x[:,currentIndex])[:,np.newaxis]
                        mse_current = np.mean((pred-valid_y)**2)

                        predT = regr.predict(test_x[:,currentIndex])[:,np.newaxis]
                        mse_t_current = np.mean((predT-test_y)**2)
                        
                        validDom = mat_hsic(valid_y, nExV)
                        Residual = valid_y-pred

                        if use_hsic:
                                ls = np_getDistances(Residual, Residual)
                                sx= 0.5*np.median(ls.flatten())
                                stat, a, b = numpy_HsicGammaTest(Residual,validDom,sx,1, DomKer = validDom)
                                pvals = 1.- sp.stats.gamma.cdf(stat, a, scale=b)
                        else:
                                residTup = levene_pval(Residual,nEx, dataset.numTasks)
                                pvals = sp.stats.levene(*residTup)[1]

                        if (pvals > delta) and (mse_current<best_mse):

                                best_mse = mse_current
                                best_mse_t = mse_t_current
                                best_subset = s

        mse[0,0] += best_mse
        mse[1,0] += best_mse**2
        mse_t[0,0] += best_mse_t
        mse_t[1,0] += best_mse_t**2

        return mse, mse_t, np.array(best_subset)


#Given alpha and exp(eps^2), compute the update from Proposition 3
def optimal_coefs(alpha,epsEst,Xc,Xe,Y,numSamples,numCauses,numEffects,reg):

	r = np.eye(numCauses)
	re = np.eye(numEffects)

	betaE, betaC = 0,alpha
	regr = linear_model.Ridge(alpha = 0.0000001)
	regr.fit(Y[:,np.newaxis], Xe)

        alphaE_estim = regr.coef_.reshape(regr.coef_.size)
        SigmaX_estimated = 1./numSamples*np.dot(Xc.T,Xc)

        SigmaXe_estimated = 1./numSamples*np.dot(Xc.T,(Xe-Y[:,np.newaxis]*alphaE_estim))
        Sigmae_estimated = 1./numSamples*np.dot((Xe-Y[:,np.newaxis]*alphaE_estim).T,(Xe-Y[:,np.newaxis]*alphaE_estim))

        sigma2_estimated = epsEst

        invX = np.linalg.inv(SigmaX_estimated + reg*r)
        M = sigma2_estimated*alphaE_estim[:,np.newaxis]*alphaE_estim
        M += Sigmae_estimated
        M -= np.dot(np.dot(invX,SigmaXe_estimated).T,SigmaXe_estimated)
        
        if alpha.size >0:
                betaE = np.dot(sigma2_estimated*alphaE_estim,np.linalg.inv(M+reg*re).T)
                Mat = np.dot(invX,np.dot(SigmaXe_estimated,betaE[:,np.newaxis]))
                Mat = Mat.reshape(Mat.size)
                betaC = alpha*(-np.dot(betaE,alphaE_estim)+1)- Mat
        else:
                betaC = 0
        
	return betaC, betaE


def tl_run_experiment(dataset, mse_error, mse_test_error,i, alpha_test):

	indexT = 0
	numTasks = dataset.numTasks
        num_causes = dataset.numCauses
        true_causal_set = np.arange(num_causes)

	for task in np.arange(3,numTasks+1):

		dataset.numTasks = task
		nEx = dataset.nEx
		
		numT = np.arange(np.sum(nEx[0:task]))

		trainX,trainY = dataset.XTrain[numT,:], dataset.YTrain[numT,:]
		validX,validY = dataset.XValid[numT,:], dataset.YValid[numT,:]
		testX, testY = dataset.XTest, dataset.YTest

		# Compute pooled errors (beta^pool)        
		regr = linear_model.LinearRegression()
		regr.fit(trainX,trainY)
		pred = regr.predict(validX)
		ev =np.mean((pred-validY)**2)

		alpha = regr.coef_
		predT = regr.predict(testX)
		evT = np.mean((testY-predT)**2)

		mse_error[0,indexT, i] += (ev)
		mse_test_error[0,indexT, i] += (evT)

		# Error for exhaustive subset search (beta^S)
		mse_subsets, mse_subsets_t,c = best_subset_regression(dataset, False, alpha_test)
                mse_error[1,indexT, i] += (mse_subsets[0][0])
		mse_test_error[1,indexT, i] += (mse_subsets_t[0][0])
	
                # Errors for ground truth (beta^cau)
                regr_tc = linear_model.LinearRegression()
                regr_tc.fit(trainX[:,true_causal_set], trainY)
                pred_tc = regr_tc.predict(validX[:,true_causal_set])
                mse_tc = np.mean((pred_tc-validY)**2)

                pred_tc_test = regr_tc.predict(testX[:,true_causal_set])
                mse_tc_test = np.mean((pred_tc_test-testY)**2)
        
                mse_error[2, indexT, i] += mse_tc
                mse_test_error[2,indexT, i] += mse_tc_test

                #Errors with mean
                pred_mean = np.mean(trainY)
                mse_mean = np.mean((pred_mean-testY)**2)
                mse_test_error[3, indexT, i] += mse_mean

		indexT += 1

	return mse_error, mse_test_error

def mtl_run_experiment(dataset, alpha, mse_error, mse_test_error, i, samp_array):
        
	indexS = 0
	numTasks = dataset.numTasks
	numSamp = dataset.nEx[0]

	nEx = dataset.nEx
        trainX,trainY = dataset.XTrain, dataset.YTrain
        validX,validY = dataset.XValid, dataset.YValid
        testX_all, testY_all = dataset.XTest, dataset.YTest

        mask = lasso_alpha_search_synt(trainX,trainY)
        dataset.XTrain = trainX[:,mask]
        dataset.XValid = dataset.XValid[:,mask]
        dataset.XTest = testX_all[:,mask]
        
        num_pred_temp = dataset.numPredictors
        dataset.numPredictors = np.where(mask==True)[0].size

        mse_subsets, mse_subsets_t, c = best_subset_regression(dataset,False, alpha)
        dataset.numPredictors = num_pred_temp
        true_causal_set = np.arange(dataset.numCauses)

	for samp in samp_array:
		
		testX, testY = testX_all[numSamp/2:numSamp,:], testY_all[numSamp/2:numSamp,:]
		testTrainX, testTrainY = testX_all[0:samp,:], testY_all[0:samp, :]

                # Error for domain indep (beta^dom)
                if samp< dataset.numPredictors:
                        regr = linear_model.RidgeCV()
                else:
                        regr = linear_model.LinearRegression()
		regr.fit(testTrainX, testTrainY)

		pred = regr.predict(validX)
		mse = np.mean((pred-validY)**2)

		predT = regr.predict(testX) 
		mse_t = np.mean((predT-testY)**2)
		mse_error[3,indexS, i] += np.mean((pred-validY)**2)
		mse_test_error[3,indexS, i] += np.mean((predT-testY)**2)

		# Error for pooled (beta^pool)
		regr = linear_model.LinearRegression()#RidgeCV()
		regr.fit(np.append(trainX,testTrainX,axis=0),np.append(trainY, testTrainY,axis=0))
		pred = regr.predict(validX)
		mse = np.mean((pred-validY)**2)
                
		alpha = regr.coef_
		predT = regr.predict(testX)
		mse_t = np.mean((predT-testY)**2)
		mse_error[0,indexS, i] += mse
		mse_test_error[0,indexS, i] += mse_t
               
                
		# Error for updated coefs (beta^S+)
		regu = 0.0001
                if samp < 2*dataset.numCauses:
                        regr = linear_model.RidgeCV()
                else:
                        regr = linear_model.LinearRegression()
                if samp > dataset.numPredictors/2:
                        regu = regu/(1+100*samp)
                if samp > dataset.numPredictors:
                        regu = 0

                if len(c) == 0:
                        pred = np.mean(validY)
                        alp = np.array([])
                else:
                        regr.fit(trainX[:,c],trainY)
                        pred = regr.predict(validX[:,c])
                        alp = regr.coef_

                epsEst = np.mean((validY-pred)**2)
                mask = np.ones(dataset.numPredictors, dtype = bool)
                c = np.array(c)

                mask[c] = False
                if c.size == dataset.numPredictors:
                        aC, aE = alp, 0
                else:
                        aC,aE = optimal_coefs(alp,epsEst,testTrainX[:,c],testTrainX[:,mask],testTrainY.flatten(),samp,len(c),dataset.numPredictors-len(c), regu)
                        aE = aE[np.newaxis,:]

                pred = np.sum(aC*validX[:,c],1) + np.sum(aE*validX[:,mask],1)
                predT = np.sum(aC*testX[:,c],1) + np.sum(aE*testX[:,mask],1)
                pred = pred[:,np.newaxis]
                predT = predT[:,np.newaxis]

                mse = np.mean((pred-validY)**2)
                mse_t =np.mean((predT-testY)**2)
                mse_error[1,indexS, i] += mse
                mse_test_error[1,indexS, i] += mse_t

                #Error for ground truth updated (beta^cau)
		regr_cau = linear_model.LinearRegression()
                regr_cau.fit(trainX[:,true_causal_set], trainY)
                pred_cau = regr_cau.predict(validX[:,true_causal_set])

                epsEst = np.mean((validY-pred_cau)**2)
                mask = np.ones(dataset.numPredictors, dtype = bool)
                mask[true_causal_set] = False
                
                aC, aE = optimal_coefs(regr_cau.coef_, epsEst, testTrainX[:,true_causal_set], testTrainX[:,mask], testTrainY.flatten(), samp, len(true_causal_set), dataset.numPredictors-len(true_causal_set), regu)
                aE = aE[np.newaxis,:]

                pred = np.sum(aC*validX[:,true_causal_set],1) + np.sum(aE*validX[:,mask],1)
                predT = np.sum(aC*testX[:,true_causal_set],1) + np.sum(aE*testX[:,mask],1)
                pred = pred[:,np.newaxis]
                predT = predT[:,np.newaxis]

                mse = np.mean((pred-validY)**2)
                mse_t =np.mean((predT-testY)**2)
                mse_error[2,indexS, i] += mse
                mse_test_error[2,indexS, i] += mse_t

		indexS += 1
				
	return mse_error, mse_test_error
